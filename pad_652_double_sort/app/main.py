# Método para ordenar os itens da lista de acordo com o seu tipagem
def double_sort(matriz: list) -> list:
    # Gerando as listas de números e strings
    list_numbers = []
    list_string = []

    # Laço repetição para transpor os itens conforme condição de restrição de tipagem
    for n in matriz:
        if type(n) is int or type(n) is float:
            list_numbers.append(n)
            list_numbers.sort()
        elif type(n) is str:
                list_string.append(n)
                list_string.sort()
    matriz_final = list_numbers + list_string
    # Retornando a lista com os daos ordenados conforme regra de negócio
    return matriz_final

#print(double_sort(['oi', 2, 2.5, 'test', '.', 234, 'nice', 3.5, '2']))
