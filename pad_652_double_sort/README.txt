Simples o suficiente - você receberá uma lista. Os valores na lista serão números ou seqüências de caracteres,
 ou uma mistura de ambos. Você não receberá uma matriz vazia, nem uma dispersa.

Seu trabalho é retornar uma única lista que tenha primeiro os números classificados em ordem crescente, seguidos pelas
cadeias classificadas em ordem alfabética. Os valores devem manter seu tipo original.

Observe que os números escritos como strings são strings e devem ser classificados com as outras strings.