import unittest
from pad_664_count_smiles.app.main import Smiles


class TestMain(unittest.TestCase):
    def test_count_smiles(self):
        count_smiles = Smiles([':D', ':~)', ';~D', ':)'])
        self.assertEqual(count_smiles.count_true_smiles(), 4)

    def test_function_count_smiles_return_integer(self):
        count_smiles = Smiles([':D', ':~)', ';~D', ':)'])
        self.assertIsInstance(count_smiles.count_true_smiles(), int)
