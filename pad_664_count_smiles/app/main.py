# class Smiles:
#     def __init__(self, faces: list):
#         self._faces = faces
#         # lista de sorrisos válidas
#         self._true_smiles = [':)', ':D', ';)', ';D', ':-)', ':-D', ';-)', ';-D', ':~)', ':~D', ';~)', ';~D']
#         # total de sorrisos válidos
#         self._total_true_smiles = 0
#
#     def count_true_smiles(self) -> int:
#         # Loop que vai percorrer a lista enviada verificando se tem sorrisos válidos
#         for i in range(0, len(self._faces)):
#             if self._faces[i] in self._true_smiles:
#                 self._total_true_smiles += 1
#         # Retorna total de sorrisos encontrados ou 0 caso não for encontrado nenhum
#         return self._total_true_smiles if self._total_true_smiles != 0 else 0
#
#
# count_smiles = Smiles([':*', ':)', ';/'])
# print(count_smiles.count_true_smiles())


class Smiles:
    def __init__(self, faces: list):
        self._faces = faces
        # lista de sorrisos válidas
        self._true_smiles = [':)', ':D', ';)', ';D', ':-)', ':-D', ';-)', ';-D', ':~)', ':~D', ';~)', ';~D']
        # total de sorrisos válidos
        self._list_true_smiles = []

    def count_true_smiles(self) -> int:
        # Loop que vai percorrer a lista enviada verificando se tem sorrisos válidos
        self._list_true_smiles = list(filter(lambda valor: valor in self._faces, self._true_smiles))
        # Retorna total de sorrisos encontrados ou 0 caso não for encontrado nenhum
        return len(self._list_true_smiles) if self._list_true_smiles != 0 else 0


count_smiles = Smiles([':*', ':-)', ':)', ';/'])
print(count_smiles.count_true_smiles())
