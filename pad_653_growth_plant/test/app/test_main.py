import unittest
from pad_653_growth_plant.app.main import Plant


class TestGrowingPlant(unittest.TestCase):
    def test_growing_plant(self):
        plant = Plant(100, 10, 910)
        self.assertEqual(plant.growing_plant(), 10)
        plant = Plant(155, 3, 200)
        self.assertEqual(plant.growing_plant(), 2)
        plant = Plant(20, 10, 90)
        self.assertEqual(plant.growing_plant(), 8)

    def test_function_growing_plant_return_integer(self):
        plant = Plant(10, 5, 500)
        self.assertIsInstance(plant.growing_plant(), int)
