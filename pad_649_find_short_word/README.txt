Simples, com uma sequência de palavras, retorne o comprimento da (s) palavra (s) mais curta (s).

A sequência nunca estará vazia e você não precisará contabilizar diferentes tipos de dados.