import unittest
from pad_649_find_short_word.app.main import find_short_word


class TestMain(unittest.TestCase):
    def test_find_short_word(self):
        self.assertEqual(find_short_word("HBSIS, a 35ª melhor empresa para se trabalhar."), 1)
        self.assertEqual(find_short_word("We're here for tech and beer."), 3)
        self.assertEqual(find_short_word("Qualidade é um hábito que transforma."), 1)

    def test_function_find_short_word_return_integer(self):
        self.assertIsInstance(find_short_word("Celebre novas possibilidades."), int)
