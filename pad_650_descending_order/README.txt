Sua tarefa é criar uma função que possa pegar qualquer número inteiro não negativo como argumento e retorná-lo
com seus dígitos em ordem decrescente. Essencialmente, reorganize os dígitos para criar o número mais alto possível.

Exemplos:
Entrada: 21445 Saída:54421

Entrada: 145263 Saída:654321

Entrada: 123456789 Saída:987654321