Desta vez, nenhuma história, nenhuma teoria. Os exemplos abaixo mostram como escrever a função accum:

Exemplos:

accum("abcd") -> "A-Bb-Ccc-Dddd"
accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
accum("cwAt") -> "C-Ww-Aaa-Tttt"
O parâmetro accum é uma string que inclui apenas letras de a..ze A..Z.