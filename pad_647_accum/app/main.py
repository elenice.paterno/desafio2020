def accum(param: str):
    initial_string = list(param)
    final_string = ''

    # Laço de repetição vai contar quantas letras tem em cada uma os parametros recebidos
    for i in range(0, len(initial_string)):
        # Variável letra calcula o número de ocorrências de cada letra conforme sua posição
        letter = (i + 1) * initial_string[i]
        # Formatar a saída
        final_string = final_string + '-' + letter
    return final_string[1:].title()


# Manual test
# print(accum('teste'))
