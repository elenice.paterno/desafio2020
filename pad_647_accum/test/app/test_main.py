import unittest
from pad_647_accum.app.main import accum


class TestMain(unittest.TestCase):
    def test_accum(self):
        self.assertEqual(accum("abcd"), "A-Bb-Ccc-Dddd")
        self.assertEqual(accum("Tarcisio"), "T-Aa-Rrr-Cccc-Iiiii-Ssssss-Iiiiiii-Oooooooo")
        self.assertEqual(accum("ELENICE"), "E-Ll-Eee-Nnnn-Iiiii-Cccccc-Eeeeeee")

    def test_function_accum_return_string(self):
        self.assertIsInstance(accum("HBSIS"), str)
