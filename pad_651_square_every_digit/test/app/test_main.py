import unittest
from pad_651_square_every_digit.app.main import square_every_digit


class TestSquareEveryDigit(unittest.TestCase):
    def test_square_every_digit(self):
        self.assertEqual(square_every_digit(123), 149)
        self.assertEqual(square_every_digit(453153), 162591259)
        self.assertEqual(square_every_digit(105), 1025)

    def test_function_descending_square_every_digit_return_integer(self):
        self.assertIsInstance(square_every_digit(123), int)
