def get_middle(word: str):
    # Impar recebe um caracter do meio da palavra
    # odd = word[length // 2]

    # Par recebe os dois caracteres do meio da palavra
    # even = word[(length - 1) // 2] + odd

    # Contagem dos caracteres como impar
    # count_character_is_odd = length % 2 != 0

    # A variavel meio da palavra recebe classificação como impar se a contagem dos caracteres for impar, se não recebe par
    middle_word = (word[len(word) // 2]) if (len(word) % 2 != 0) else (word[(len(word) - 1) // 2] + word[len(word) // 2])
    return middle_word


# Manual Test
# print(get_middle('Tarcisio'))
