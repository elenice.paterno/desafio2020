import unittest
from pad_646_get_middle.app.main import get_middle


class TestMain(unittest.TestCase):
    def test_get_middle(self):
        self.assertEqual(get_middle("Tarcisio"), "ci")
        self.assertEqual(get_middle("Elenice"), "n")
        self.assertEqual(get_middle("1254632"), "4")

    def test_function_get_middle_return_string(self):
        self.assertIsInstance(get_middle("HBSIS"), str)
