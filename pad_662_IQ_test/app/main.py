# def iQ_test(list_int: list) -> int:
#     even = []
#     odd = []
#     # Ação de percorrer a lista
#     for n in list_int:
#         # Verifica se o número é par ou impar e adiciona em sua lista correspondente
#         even.append(n) if n % 2 == 0 else odd.append(n)
#     # Verifica qual a maior lista e a retorna
#     return list_int.index(odd[0]) + 1 if len(odd) < len(even) else list_int.index(even[0]) + 1
#
# # print(iQ_test([1, 5, 9, 8, 11]))

def iQ_test(list_int: list) -> int:
    # Cria duas listas a partir da lista original utilizando o o filter
    even = list(filter(lambda valor: valor % 2 == 0, list_int))
    odd = list(filter(lambda valor: valor % 2 != 0, list_int))
    # Verifica qual a maior lista e a retorna
    return list_int.index(odd[0]) + 1 if len(odd) < len(even) else list_int.index(even[0]) + 1

print(iQ_test([1, 5, 9, 8, 11]))

