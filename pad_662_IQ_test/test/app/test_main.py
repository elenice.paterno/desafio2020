import unittest
from pad_662_IQ_test.app.main import iQ_test


class TestMain(unittest.TestCase):
    def test_iQ(self):
        self.assertEqual(iQ_test([2, 4, 7, 8, 10]), 3)
        self.assertEqual(iQ_test([2, 2, 2, 4, 1]), 5)
        self.assertEqual(iQ_test([5, 4, 8, 4, 0]), 1)

    def test_function_iQ_return_integer(self):
        self.assertIsInstance(iQ_test([2, 2, 2, 4, 1]), int)
