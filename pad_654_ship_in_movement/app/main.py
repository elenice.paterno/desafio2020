from math import sin, radians


def find_time_to_break(angle: int, angle2: int) -> float:
    # if angle == 0 and angle2 == 0:
    #     return float('inf')

    angle_difference = abs(angle - angle2)
    angle_sum = 180
    gama = angle_sum - angle_difference / 2
    distance_limit = 40 * 1.609344
    velocity = 150

    distance = distance_limit * sin(radians(gama)) / sin(radians(angle_difference))
    temp = distance / velocity
    temp_minutes = temp * 60

    return round(temp_minutes, 2)

# Manual test
# print(find_time_to_break(0, 90))
