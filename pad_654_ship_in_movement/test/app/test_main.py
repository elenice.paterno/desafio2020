import unittest
from pad_654_ship_in_movement.app.main import find_time_to_break


class TestMain(unittest.TestCase):
    def test_find_time_to_break(self):
        self.assertEqual(find_time_to_break(0, 90), 18.21)
        # self.assertEqual(find_time_to_break(0, 90), 18.86) -  Teste orignal

    def test_function_find_time_to_break_return_integer(self):
        self.assertIsInstance(find_time_to_break(0, 90), float)
